# company-discovery-platform
### NOTE: (I did this project as a task given to me for an Internship applied at redwhale.in)
UI for a company discovery platform where companies can create profiles to showcase their company culture and achievements.  

## Companies can upload  
1. Cover image 
2. Logo 
3. Location 
4. Short description 
5. Highlights – (Icons with label) 
6. Images and videos to showcase company culture 
7. Long description 
8. Team 
9. Milestones/Timeline 
10. Description about company products/services 
11. Job vacancies 
12. Contact details, website. Social media links

## See live  here:

https://i-shubhamprakash.github.io/company-discovery-platform-ui/
